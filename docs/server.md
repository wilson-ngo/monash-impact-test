# Server

The following instructions are for setting up a [Bitnami WordPress VM](https://bitnami.com/stack/wordpress/virtual-machine) to deploy to. These should work for EC2-deployed VMs, as well as for local development.

As with deployment, the instructions below assume you’re started at the root of this repository and that you’ve downloaded the per-environment keys to `./config/deploy/#{environment}/*`.

## Preparing for deployment

Connect to the server:

```
ssh -i ./config/deploy/production/KP-V20-PRD-WP-BUSECO01.pem bitnami@13.54.20.101
```

Update `apt-get`:

```
sudo apt-get update
```

Install git:

```
sudo apt-get install -y git
```

Install `node` 6.x and update `npm` to the latest version:

```
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g npm
```

Adjust timezone PHP settings:

```
vi /opt/bitnami/php/etc/php.ini
```

Edit the `date.timezone` to:

```
date.timezone = "Australia/Melbourne"

```

Edit the Apache config to enable some features:

```
vi /opt/bitnami/apache2/conf/httpd.conf
```

Enable `mod_expires` headers by uncommenting:

```
LoadModule expires_module modules/mod_expires.so
```

Explicitly load our `.htaccess` file in the right location by:

```
vi ~/apps/wordpress/conf/httpd-app.conf
```

Adding the following immediately after `Include "/opt/bitnami/apps/wordpress/conf/banner.conf"`

```
    # Customisation to allow for .htaccess file to exist
    # in the codebase
    Include "/home/bitnami/apps/wordpress/htdocs/.htaccess"
```

Disable Bitnami’s information banner:

```
sudo /opt/bitnami/apps/wordpress/bnconfig --disable_banner 1
```

Make the base directory for the app to be installed:

```
sudo mkdir ~/apps/monash-impact
sudo chown bitnami:bitnami ~/apps/monash-impact
```

Move the default WordPress install aside:

```
sudo mv ~/apps/wordpress/htdocs ~/apps/wordpress/htdocs.old
```


## First deploy

Run deploy:

```
bundle exec cap production deploy
```

This will create some structures, but will fail because there’s no `~/apps/monash-impact/shared/.env`. Add the `.env` at that location with the credentials you want and run the deploy again:

```
bundle exec cap production deploy
```

After it succeeds:

```
sudo ln -s ~/apps/monash-impact/current/html ~/apps/wordpress/htdocs
sudo chown -R daemon:daemon ~/apps/monash-impact/shared/html/app/uploads
sudo chown -R daemon:daemon ~/apps/monash-impact/shared/vendor/timber/timber
```

## Database

You can connect to the database from your local machine using an SSH tunnel:

```
ssh -L 127.0.0.1:13306:127.0.0.1:3306 -i ./config/deploy/production/KP-V20-PRD-WP-BUSECO01.pem bitnami@13.54.20.101
```