# Deployment

Deployment is managed through an automated process built on top of [Capistrano](http://capistranorb.com/). You can read more about [why and how Capistrano works on their README](https://github.com/capistrano/capistrano/blob/master/README.md#features).

You’ll need ruby 2.4 installed and [bundler](http://bundler.io/) installed on your machine to run these tasks.

## Installation

Once you have ruby/bundler set up you can run:

```
bundle
```

To install capistrano and its dependencies.

You’ll need to download the Bitnami credentials for any servers you want to deploy to and place them in: `./config/deploy/#{environment}/*`. The credentials for the production server are currently expected at:

```
./config/deploy/production/KP-V20-PRD-WP-BUSECO01.pem
```

Note that these keys are explicitly _excluded_ from the git repository. You’ll need to download them separately.

## Stages

You can specify different stages to deploy to. There are three included here by default:

* development
* staging
* production

You can target a capistrano task to a specific stage thusly:

```
bundle exec cap #{stage} ...
```

So to deploy to production, for example, you would:

```
bundle exec cap production deploy
```

All the examples from here on will reference `production` only as the deployment stage, but you can substitute any other stage to target it. The stages themselves can be configured in `./config/deploy`.

Global configuration — i.e., configuration shared between all stages — is in `./config/deploy.rb`. Here you can adjust:

* The git repository used to deploy from
* The branch of that repository to use
* The number of releases to keep historically (for rollback)
* Any other settings that capistrano allow to change ...

## Deployment tasks

### Deploying

To deploy the application run:

```
bundle exec cap production deploy
```

This will:

1. Checkout the latest version of the repository/branch specified in `./config/deploy.rb` into `./tmp/deploy`.
2. Sync that to the server using `rsync`.
3. Run any additional tasks: composer install, installing and building assets with npm.
4. Copy the code into a new timestamped folder under `~/apps/monash-impact/releases`.
5. Symlink `~/apps/monash-impact/current` to the latest release.
6. Restart both apache and php-fpm.

If any of the first four steps fail, the deployment will be aborted and the old release will continue to be used.

Note: We use rsync as the SCM strategy here because Monash’s git repositories are protected behind a VPN. You must be connected to the VPN to be able to run the deployment.

### Rollback

If a release has an error after deployment that you want to undo, you can roll it back:

```
bundle exec cap production deploy:rollback
```

This will rollback to the release before the current release. You can also rollback to a _specific_ release thusly:

```
bundle exec cap production deploy:rollback ROLLBACK_RELEASE=#{release_timestamp}
```

Where the value of `release_timestamp` is one of the release folders visible at `~/apps/monash-impact/releases` on the server you’re targeting. Capistrano is currently configured to keep the last 10 releases for rollback.

### Other tasks

You can view all available deployment tasks using:

```
bundle exec cap -T
```

## Persistent data

Files that need to persist between deployments are kept in `~/apps/monash-impact/shared/*` and symlinked into each release in a matching file/folder structure. These include:

* `.env` file with the ENV variables and secrets for the application.
* File uploads from the WordPress application.
* Any file caching from the Timber templating system used in the WordPress application.