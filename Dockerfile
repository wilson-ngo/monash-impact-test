FROM php:7-apache

# Install some core tools and development packages
RUN apt-get update && apt-get install -y --no-install-recommends \
  apt-transport-https \
  build-essential \
  bzip2 \
  ca-certificates \
  curl \
  git \
  openssh-client \
  wget

# Install mod_*
RUN a2enmod rewrite
RUN a2enmod headers
RUN a2enmod expires
RUN a2enmod deflate
RUN a2enmod filter
RUN a2enmod mime

# Install PHP zip extension for composer
RUN apt-get update && apt-get install -y --no-install-recommends \
    zlib1g-dev \
    && docker-php-ext-install zip

# Install MySQL extension for WordPress
RUN docker-php-ext-install mysqli

# Install PHP GD library and its dependencies
RUN apt-get install -y --no-install-recommends \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng12-dev \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

# Install node from custom apt-get sources, then purge apt- get data we don't
# need anymore

RUN \
  echo deb https://deb.nodesource.com/node_6.x trusty main > /etc/apt/sources.list.d/nodesource.list \
  && wget -qO- https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - \
  && apt-get update \
  && apt-get install -y nodejs \
  && rm -rf /var/lib/apt/lists/*

# Install composer
RUN bash -c "curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer"

WORKDIR /var/www

# Copy files
COPY php.ini /usr/local/etc/php/php.ini
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ./html/ /var/www/html
COPY ./config/ /var/www/config/

# Install composer dependencies
WORKDIR /var/www
COPY ./composer.json /var/www
COPY ./composer.lock /var/www
RUN composer install

# Install NPM modules
#
# Installs to the directory above the project so that
# modules will still be found but won't be overridden when mounting the
# project directory for development
#
# Copy any package.json and webpack config overrides (we need to use the
# wildcard here and on the same line in case there is none)
COPY package.json webpack.config.* /var/www/
WORKDIR /var/www
RUN npm install
RUN npm run build

# Add the node_modules to the PATH, so we can call any node commands directly
ENV PATH /var/www/node_modules/.bin:$PATH

# Set permissions
RUN chown -R www-data:www-data /var/www/html
RUN chown -R www-data:www-data /var/www/vendor/timber/timber

# Expose Apache
EXPOSE 80
