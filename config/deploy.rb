set :application, "monash-impact"
set :repo_url, "https://bitbucket.org/wilson-ngo/monash-impact-test.git"

# Branch options
# Prompts for the branch name (defaults to current branch)
#ask :branch, -> { `git rev-parse --abbrev-ref HEAD`.chomp }

# Hardcodes branch to always be master
# This could be overridden in a stage config file
set :branch, :master

# Use rsync for deployment
set :scm, :rsync
set :rsync_scm, :git

set :rsync_options, %w[
  --recursive
  --delete
  --delete-excluded
  --exclude .git*
  --exclude bitnami*
  --exclude .env*
  --exclude *.sql
  --exclude *.pem
  --exclude *.key
]

set :deploy_to, -> { "/home/bitnami/apps/#{fetch(:application)}" }

# Use :debug for more verbose output when troubleshooting
set :log_level, :info

# Apache users with .htaccess files:
# it needs to be added to linked_files so it persists across deploys:
set :linked_files, fetch(:linked_files, []).push('.env')
set :linked_dirs, fetch(:linked_dirs, []).push('html/app/uploads', 'vendor/timber/timber')

# Default value for keep_releases is 5
set :keep_releases, 10

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :sudo, "/opt/bitnami/ctlscript.sh restart php-fpm"
      execute :sudo, "/opt/bitnami/ctlscript.sh restart apache"
    end
  end
end

namespace :assets do
  desc "Install node_modules"
  task :install_modules do
    on roles(:app), in: :sequence do
      execute "cd #{release_path} && npm install --loglevel warn"
    end
  end

  desc "Build assets with npm"
  task :build do
    on roles(:app), in: :sequence do
      execute "cd #{release_path} && npm run build"
    end
  end
end

before "assets:build", "assets:install_modules"
after "composer:run", "assets:build"
after "deploy:publishing", "deploy:restart"