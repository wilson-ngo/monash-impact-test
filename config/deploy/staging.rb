set :stage, :staging

# Simple Role Syntax
# ==================
#role :app, %w{deploy@example.com}
#role :web, %w{deploy@example.com}
#role :db,  %w{deploy@example.com}

# Extended Server Syntax
# ======================
server '34.196.53.177', user: 'bitnami', roles: %w{web app db}

# Explicitly pass key to rsync options
key_path = "./config/deploy/staging/KP-V20-DEV-WP-MBS01.pem"
set :rsync_options, fetch(:rsync_options).push("--rsh='ssh -i #{File.join("../../", key_path)}'")
set :ssh_options, {
  keys: [key_path],
  forward_agent: true
}

fetch(:default_env).merge!(wp_env: :staging)
