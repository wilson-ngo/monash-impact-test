<?php
  /**
   * Check if a URL is active based on the path
   * @param string $check      The class URL to check
   * @param string $full_match Whether to match the entire path or not (false by default)
   * @return boolean
   */
  function is_active($check, $full_match = false) {
    $check_url = parse_url($check);
    $server_url = parse_url($_SERVER['REQUEST_URI']);
    if ($full_match) {
      return $server_url['path'] == $check_url['path'];
    } else {
      return Timber\TextHelper::starts_with(
        $server_url['path'],
        $check_url['path']
      );
    }
  }

  /**
   * Expose asset_path helper as a global for Timber/Twig
   * Use as {{asset_path, 'asset__name.ext'}}
   */
  function asset_path($filename, $full_path = false, $with_domain = false) {
    return Icelab\Assets\asset_path($filename, $full_path, $with_domain);
  }

  /**
   * Render errors
   */
  function render_error($status) {
    $context = Timber::get_context();
    if ($status == '403')
      $context['page_title'] = 'Forbidden';
    elseif ($status == '500') {
      $context['page_title'] = 'Something went wrong';
    } else {
      $context['page_title'] = 'Page not found';
    }
    Timber::render('error.twig', $context);
  }

  /**
   * Determine whether any articles of $type exist
   */
  function articles_exist($type) {
    $args = [
      'post_type' => 'article',
      'posts_per_page' => -1,
      'meta_key' => 'type',
      'meta_value' => $type,
      'meta_compare' => 'LIKE'
    ];
    $posts = Timber::get_posts($args);
    $numberOfPosts = count($posts);
    if ($numberOfPosts > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Return appropriate Timber Cache constant based on config
   */
  function cache_method() {
    if (ENABLE_CACHE) {
      return Timber\Loader::CACHE_TRANSIENT;
    } else {
      return Timber\Loader::CACHE_NONE;
    }
  }