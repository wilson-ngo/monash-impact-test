<?php

/*
  * Autoload any psr-4 definitions set in `composer.json`. By default this will
  * load anything in the `monash-impact/decorators` directory as long as the
  * classes defined there match the filenames.
  *
  * — MonashImpact\Decorators\Article —> monash-impact/decorators/article.php
  * — MonashImpact\Decorators\ContentPage —> monash-impact/decorators/content-page.php
  */
require(__DIR__ . '/../../../../vendor/autoload.php');

// Require lib
require_once(__DIR__ . '/lib/rich_text_modifier.php');

/*
  * Require decorators
  * FIXME this shouldn't be needed if the autoload worked properly
  */
require_once(__DIR__ . '/decorators/article.php');
require_once(__DIR__ . '/decorators/category.php');
require_once(__DIR__ . '/decorators/contentpage.php');
require_once(__DIR__ . '/decorators/homepage.php');
require_once(__DIR__ . '/decorators/person.php');
require_once(__DIR__ . '/decorators/tag.php');
require_once(__DIR__ . '/decorators/topic.php');

/* ------------------------------------------------------------------------- **
 * Timber/Twig configuration
 * ------------------------------------------------------------------------- */

/**
 * Initialise Timber
 */
$timber = new \Timber\Timber();

/**
 * Create global Timber context
 *
 * These variables are made available in _every_ Twig template, and so any
 * globally useful settings or information can be included here.
 */
add_filter('timber_context', 'add_to_context');
function add_to_context($context) {
  $context['development'] = (getenv('WP_ENV') == 'development') ? 1 : 0;
  // Expose options
  $options = get_fields('option');
  $context['options'] = $options;
  // Expose the full current URL based on the `WP_HOME` setting
  $context['current_url'] = getenv('WP_HOME') . $_SERVER['REQUEST_URI'];
  $context['is_single'] = is_single();

  // Navigation topics
  $topic_args = [
    'post_type' => 'topic',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'ASC'
  ];
  $context['topics'] = Timber::get_posts($topic_args, 'MonashImpact\Decorators\Topic');

  // Promo banner
  $promoBanner = get_field('promo_banner', 'options');
  $context['promo_banner'] = $promoBanner[0];

  // Social URLs
  $context['twitter_url'] = 'https://twitter.com/' . get_field('twitter_username', 'options');
  $context['facebook_url'] = get_field('facebook_url', 'options');
  $context['linkedin_url'] = get_field('linkedin_url', 'options');

  // Check for articles of type
  $context['podcasts'] = articles_exist('podcast');
  $context['infographics'] = articles_exist('infographic');
  $context['videos'] = articles_exist('video');

  // Header and footer blurbs
  $context['header_blurb'] = get_field('header_blurb', 'options');
  $context['footer_blurb'] = get_field('footer_blurb', 'options');

  return $context;
}

/**
 * Custom Twig extensions
 * <https://twig.sensiolabs.org/doc/2.x/advanced.html>
 */
add_filter('get_twig', 'add_to_twig');
function add_to_twig($twig) {
  // Add any custom filters
  // $twig->addFilter(new Twig_SimpleFilter('whatever', 'my_whatever'));
  // Add any custom functions
  $twig->addFunction(new Twig_Function('asset_path', 'asset_path'));
  $twig->addFunction(new Twig_Function('is_active', 'is_active'));
  $twig->addFunction(new Twig_Function('number_of_grid_columns', 'number_of_grid_columns'));
	return $twig;
}

/* ------------------------------------------------------------------------- **
 * Advanced Custom Fields
 * ------------------------------------------------------------------------- */

/**
 * Add ACF options pages
 */
if(function_exists('acf_add_options_page')) {
  acf_add_options_page();
  acf_add_options_sub_page('Home page');
  acf_add_options_sub_page('Site settings');
}

/**
 * Enable local JSON
 */
function acf_json_save_point( $path ) {
  $path = get_stylesheet_directory() . '/fields';
  return $path;
}
add_filter('acf/settings/save_json', 'acf_json_save_point');

/**
 * Add article relationship fields to the search index
 * https://www.relevanssi.com/knowledge-base/advanced-custom-fields-relationship-fields/
 */
function add_authors_to_search_index($content, $post) {
  $authors = get_field('authors', $post->ID);
  if($authors) {
    foreach ($authors as $author) {
      $content .= $author->post_title;
      $content .= ' ';
    }
  }
  return $content;
}
add_filter('relevanssi_content_to_index', 'add_authors_to_search_index', 10, 2);
add_filter('relevanssi_excerpt_content', 'add_authors_to_search_index', 10, 2);

function add_tags_to_search_index($content, $post) {
  $tags = get_field('tags', $post->ID);
  if($tags) {
    foreach ($tags as $tag) {
      $content .= $tag->name;
      $content .= ' ';
    }
  }
  return $content;
}
add_filter('relevanssi_content_to_index', 'add_tags_to_search_index', 10, 2);
add_filter('relevanssi_excerpt_content', 'add_tags_to_search_index', 10, 2);

function add_academics_to_search_index($content, $post) {
  $academics = get_field('academics', $post->ID);
  if($academics) {
    foreach ($academics as $academic) {
      $content .= $academic->name;
      $content .= ' ';
    }
  }
  return $content;
}
add_filter('relevanssi_content_to_index', 'add_academics_to_search_index', 10, 2);
add_filter('relevanssi_excerpt_content', 'add_academics_to_search_index', 10, 2);

/**
 * Customize WYSIWYG editor
 */
function adjust_wysiwyg_toolbars($toolbars) {
  // Unset existing toolbars
  unset($toolbars['Basic']);
  unset($toolbars['Full']);

  // Create new 'Basic' toolbar
  $toolbars['Basic'] = array();
  $toolbars['Basic'][1] = array('bold','italic','link','unlink','bullist','numlist','undo','redo','fullscreen');

  // Create new 'Full' toolbar
  $toolbars['Full'] = array();
  $toolbars['Full'][1] = array('formatselect','bold','italic','blockquote','bullist','numlist','alignleft','alignright','link','unlink','undo','redo','fullscreen');

  return $toolbars;
}
add_filter('acf/fields/wysiwyg/toolbars', 'adjust_wysiwyg_toolbars');

/*
 * Paste as plain text
 */
function tinymce_paste_as_text($init) {
  $init['paste_as_text'] = true;
  return $init;
}
add_filter('tiny_mce_before_init', 'tinymce_paste_as_text');

/*
 * Modify TinyMCE formatting dropdown
 */
function tiny_mce_remove_unused_formats($init) {
  $init['block_formats'] = 'Paragraph=p;Heading 1=h1;Heading 2=h2;Heading 3=h3';
  return $init;
}
add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats');

/* ------------------------------------------------------------------------- **
 * WordPress behaviour configuration
 * ------------------------------------------------------------------------- */

/**
 * Remove necessary <meta> tags generated by wp_head();
 */
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head',10,0);

/**
 * Remove JSON API headers
 */
remove_action('template_redirect', 'rest_output_link_header', 11, 0);

/**
 * Hide the admin toolbar
 */
function my_function_admin_bar() {
  return false;
}
add_filter('show_admin_bar', 'my_function_admin_bar');

/**
 * Remove URL hash (#) to avoid jump links
 */
function remove_more_jump_link($link) {
  $offset = strpos($link, '#more-');
  if ($offset) {
    $end = strpos($link, '"',$offset);
  }
  if ($end) {
    $link = substr_replace($link, '', $offset, $end-$offset);
  }
  return $link;
}
add_filter('the_content_more_link', 'remove_more_jump_link');

/**
 * Replace the current site URL with links to the canonical domain
 * This is to handle WordPress' ridiculous tendency to include the
 * fully-qualified domain+protocol everywhere.
 */
function rewrite_permalinks_to_canonical_domain($permalink) {
  $url = getenv('WP_SITEURL');
  $pattern = "/".preg_quote($url, "/")."/i";
  $replacement = "http://" . getenv('WP_HOME');
  $permalink = preg_replace($pattern, $replacement, $permalink);
  return $permalink;
};
add_filter("post_link", "rewrite_permalinks_to_canonical_domain");
add_filter("page_link", "rewrite_permalinks_to_canonical_domain");
add_filter("post_type_link", "rewrite_permalinks_to_canonical_domain");
add_filter("_get_page_link", "rewrite_permalinks_to_canonical_domain");
add_filter("attachment_link", "rewrite_permalinks_to_canonical_domain");
add_filter("year_link", "rewrite_permalinks_to_canonical_domain");
add_filter("month_link", "rewrite_permalinks_to_canonical_domain");
add_filter("day_link", "rewrite_permalinks_to_canonical_domain");
add_filter("the_feed_link", "rewrite_permalinks_to_canonical_domain");
add_filter("post_comments_feed_link", "rewrite_permalinks_to_canonical_domain");
add_filter("author_feed_link", "rewrite_permalinks_to_canonical_domain");
add_filter("tag_feed_link", "rewrite_permalinks_to_canonical_domain");
add_filter("taxonomy_feed_link", "rewrite_permalinks_to_canonical_domain");
add_filter("search_link", "rewrite_permalinks_to_canonical_domain");
add_filter("search_feed_link", "rewrite_permalinks_to_canonical_domain");
add_filter("post_type_archive_link", "rewrite_permalinks_to_canonical_domain");
add_filter("post_type_archive_feed_link", "rewrite_permalinks_to_canonical_domain");
add_filter("next_post_link", "rewrite_permalinks_to_canonical_domain");
add_filter("previous_post_link", "rewrite_permalinks_to_canonical_domain");
add_filter("get_pagenum_link", "rewrite_permalinks_to_canonical_domain");
add_filter("shortcut_link", "rewrite_permalinks_to_canonical_domain");

/**
 * Hide update notifications
 * http://www.wpoptimus.com/626/7-ways-disable-update-wordpress-notifications/
 */
add_action('after_setup_theme','remove_core_updates');
function remove_core_updates() {
  if(!current_user_can('update_core')){ return; }
  add_action(
    'init',
    create_function('$a',"remove_action('init', 'wp_version_check');"),
    2
  );
  add_filter('pre_option_update_core','__return_null');
  add_filter('pre_site_transient_update_core','__return_null');
}
remove_action('load-update-core.php','wp_update_plugins');

/**
 * Add support for post thumbnails
 * <https://codex.wordpress.org/Post_Thumbnails>
 */
add_theme_support('post-thumbnails');

/**
 * Hide all the default post types from the WP admin nav
 * This includes `posts` and `pages`.
 *
 * WordPress is insane, it seems like it’s impossible to target "post" by
 * themselves using `register_post_type_args`. We should instead simply
 * avoid _all_ the standard WP post types and replace them with custom ones.
 */
function remove_default_post_type_menus() {
  // Remove posts
  remove_menu_page('edit.php');
  // Remove pages
  remove_menu_page('edit.php?post_type=page');
  // Remove comments
  remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'remove_default_post_type_menus');

function remove_default_post_type_add_new() {
    global $wp_admin_bar;
    // Remove posts
    $wp_admin_bar->remove_node('new-post');
    // Remove pages
    $wp_admin_bar->remove_node('new-page');
    // Remove comments
    $wp_admin_bar->remove_node('comments');
}
add_action( 'admin_bar_menu', 'remove_default_post_type_add_new', 999 );

/**
 * Nuclear option to blow away _all_ WPs default rewrite rules
 * This is probably a bad idea
 */
function remove_rewrite_rules($rules) {
  return [];
};
add_filter('rewrite_rules_array', 'remove_rewrite_rules', 10, 1);

/* ------------------------------------------------------------------------- **
 * Helpers
 * ------------------------------------------------------------------------- */

require('helpers/index.php');

/* ------------------------------------------------------------------------- **
 * Routing
 * ------------------------------------------------------------------------- */

require('routes/index.php');

/* ------------------------------------------------------------------------- **
 * Custom post types
 * ------------------------------------------------------------------------- */

require('post-types/article.php');
require('post-types/content-page.php');
require('post-types/person.php');
require('post-types/category.php');
require('post-types/topic.php');
