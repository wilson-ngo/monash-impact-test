<?php

/* Template Name: Infographics */

$context = Timber::get_context();
$context['page_title'] = $params['title'];

$args = [
  'post_type' => 'article',
  'posts_per_page' => $params['per_page'],
  'paged' => $params['page'],
  'meta_key' => 'type',
  'meta_value' => 'infographic'
];
// THIS LINE IS CRUCIAL
// https://github.com/timber/timber/wiki/Pagination
// in order for WordPress to know what to paginate
// your args have to be the default query
// make sure you've got query_posts in your .php file
query_posts($args);
// END
$context['articles'] = Timber::get_posts($args, 'MonashImpact\Decorators\Article');
$context['pagination'] = Timber::get_pagination(array('format' => 'page/%#%'));
if($context['articles']) {
  Timber::render('articles/index.twig', $context, 10800, cache_method());
} else {
  render_error('404');
}
