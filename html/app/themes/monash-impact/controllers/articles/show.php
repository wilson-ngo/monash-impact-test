<?php
/* Template Name: Articles */
$context = Timber::get_context();
$args = [
  'post_type' => 'article',
  'name' => $params['slug'],
  'meta_key' => 'type',
  'meta_value' => 'article'
];
// THIS LINE IS CRUCIAL
// https://github.com/timber/timber/wiki/Pagination
// in order for WordPress to know what to paginate
// your args have to be the default query
// make sure you've got query_posts in your .php file
query_posts($args);
// END
$context['article'] = Timber::get_post($args, 'MonashImpact\Decorators\Article');
if($context['article']) {
  $context['page_title'] = $context['article']->post_title;
  $context['related_articles'] = find_related_articles($params['slug']);
}

function find_related_articles($currentArticleSlug) {
  // Grab primary topic ID from custom field
  $topics = get_field('topics');
  $primaryTopicID = $topics[0]->ID;
  // Find ID of current article
  $article_args = array(
    'post_type' => 'article',
    'name' => $currentArticleSlug,
    'posts_per_page' => 1
  );
  $currentArticle = Timber::get_post($article_args, 'MonashImpact\Decorators\Article');
  $currentArticleID = $currentArticle->ID;
  // Query articles
  $topic_args = [
    'post_type' => 'article',
    'posts_per_page' => 4,
    'meta_key' => 'topics',
    'meta_value' => $primaryTopicID,
    'meta_compare' => 'LIKE',
    'post__not_in' => array($currentArticleID)
  ];
  // Return TimberPost
  return Timber::get_posts($topic_args, 'MonashImpact\Decorators\Article');
}

if($context['article']) {
  Timber::render('articles/show.twig', $context, 10800, cache_method());
} else {
  render_error('404');
}
