<?php
/* Template Name: Tags */

$context = Timber::get_context();

// Fetch tag by $params["slug"]
$slug = $params['slug'];
$tagArgs = [
  'slug' => $slug
];
$tags = Timber::get_terms('tag', $tagArgs, 'MonashImpact\Decorators\Tag');

if($tags) {
  $tag = $tags[0];
  $tagSlug = $tag->slug;

  // Set default value for `page` variable
  if (!isset($params['page']) || !$params['page']){
    $params['page'] = 1;
  }

  // Fetch articles with tag
  $articles_args = [
    'post_type' => 'article',
    'posts_per_page' => 16,
    'paged' => $params['page'],
    'tax_query' => array(
      array(
        'taxonomy' => 'post_tag',
        'field' => 'slug',
        'terms' => $tagSlug
      ),
    )
  ];
  query_posts($articles_args);
  $articles = Timber::get_posts($articles_args, 'MonashImpact\Decorators\Article');

  // Assemble context
  $context['tag'] = $tag;
  $context['articles'] = $articles;
  $context['page_title'] = 'Articles tagged ‘'. $tag->name . '’';
  $context['pagination'] = Timber::get_pagination(array('format' => 'page/%#%'));
  Timber::render('tags/show.twig', $context, 10800, cache_method());
} else {
  render_error('404');
}
