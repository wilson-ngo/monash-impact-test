<?php

// Globals
global $wp_query;
global $paged;

/**
 * Return search results from query string search keyword
 * @param  string $query    Search query
 * @param  int    $perPage Number of results per page
 * @param  int    $paged    Current page
 * @param  bool   $filtered Should the search results be filtered
 *
 * @return array            Search results
 */
function search_results_from_query($query, $perPage, $paged, $filtered = false) {
  // Search arguments
  $searchArgs = [
    'post_type' => 'article',
    's' => $query,
    'posts_per_page' => $perPage,
    'paged' => $paged
  ];
  // Check whether to filter the results
  $filterArgs = build_selected_filters();
  if ($filterArgs && $filtered) {
    $searchArgs = $searchArgs + $filterArgs;
  }
  // Perform the search query
  // We need to run the args through `WP_Query` because Relevanssi
  // will hijack the 's' query value for Timber::get_posts
  // https://stackoverflow.com/questions/16596812/wordpress-wp-query-using-s-value
  $searchQuery = new WP_Query($searchArgs);
  query_posts($searchArgs);
  return relevanssi_do_query($searchQuery);
}

/**
 * Generate page title
 * @param  string $query Search query
 *
 * @return string        Page title
 */
function generate_page_title($query) {
  if ($query !== NULL && $query !== '') {
    return 'Search results for ‘' . htmlspecialchars($query, ENT_QUOTES) . '’';
  } else {
    return 'Search';
  }
}

/**
 * Return a unique array of IDs from a relationship field present in search results
 * @param  array  $result Search results array
 * @param  string $field  Name of custom field
 *
 * @return array          A unique array
 */
function extract_values_from_search_results($results, $field) {
  $fieldResultIDs = array();
  foreach ($results as $result) {
    $resultID = $result->ID;
    $fieldResults = get_field($field, $resultID);
    if ($fieldResults) {
      foreach ($fieldResults as $fieldResult) {
        $fieldResultIDs[] = $fieldResult->ID;
      }
    }
  }
  return array_unique($fieldResultIDs);
}

/**
 * Build "type" filter
 * @param  array $result Search results array
 *
 * @return array An array of article types
 */
function build_type_filter($results) {
  return [
    [
      'name' => 'Article',
      'value' => 'article',
      'count' => calculate_total_results_of_type($results, 'Article')
    ],
    [
      'name' => 'Infographic',
      'value' => 'infographic',
      'count' => calculate_total_results_of_type($results, 'Infographic')
    ],
    [
      'name' => 'Podcast',
      'value' => 'podcast',
      'count' => calculate_total_results_of_type($results, 'Podcast')
    ],
    [
      'name' => 'Video',
      'value' => 'video',
      'count' => calculate_total_results_of_type($results, 'Video')
    ]
  ];
}

/**
 * Build "author" filter
 * @param  array $results Search results
 *
 * @return array          A TimberPost array of authors
 */
function build_author_filter($results) {
  $authorArgs = [
    'post_type' => 'person',
    'posts_per_page' => -1,
    'post__in' => extract_values_from_search_results($results, 'authors'),
    'orderby' => 'title',
    'order' => 'ASC'
  ];
  return Timber::get_posts($authorArgs, 'MonashImpact\Decorators\Person');
}

/**
 * Build "topic" filter
 * @param  array $results Search results
 *
 * @return array          A TimberPost array of topics
 */
function build_topic_filter($results) {
  $topicArgs = [
    'post_type' => 'topic',
    'posts_per_page' => -1,
    'post__in' => extract_values_from_search_results($results, 'topics'),
    'orderby' => 'title',
    'order' => 'ASC'
  ];
  return Timber::get_posts($topicArgs, 'MonashImpact\Decorators\Topic');
}

/**
 * Return a count for each "topic" in search results
 * @param  array  $results Search results
 * @param  string $type    Article type
 *
 * @return int             Count
 */
function calculate_total_results_of_type($results, $type) {
  $typeArray = array();
  foreach ($results as $result) {
    $resultID = $result->ID;
    $typeArray[] = get_field('type', $resultID);
  }
  $typeArrayCount = array_count_values($typeArray);
  if (isset($typeArrayCount[$type])) {
    return $typeArrayCount[$type];
  } else {
    return 0;
  }
}

/**
 * Build selected filters
 *
 * @return array A `meta_query` array to filter search results
 */
function build_selected_filters() {
  $filtersMapping = [
    [
      "paramKey" => "author",
      "metaKey" => "authors",
      "compare" => "LIKE"
    ],
    [
      "paramKey" => "topic",
      "metaKey" => "topics",
      "compare" => "LIKE"
    ],
    [
      "paramKey" => "post_type",
      "metaKey" => "type",
      "compare" => "IN"
    ]
  ];
  // Map over the filters defined above and munge into the format we want
  $filters = array_map(function ($mapping) {
    // Check if the key exists AND that it’s not empty
    $filter_active = (
      isset($_GET[$mapping["paramKey"]]) &&
      !empty($_GET[$mapping["paramKey"]])
    );
    if ($filter_active) {
      return [
          "key" => $mapping["metaKey"],
          "value" => $_GET[$mapping["paramKey"]],
          "compare" => $mapping["compare"]
        ];
    } else {
      return false;
    }
  }, $filtersMapping);
  // Filter out false values
  $filtersArray = array_filter($filters);
  // Build WordPress `meta_query`
  $metaQueryArray = [
    'meta_query' => [
      $filtersArray
    ]
  ];
  if ($filtersArray) {
    return $metaQueryArray;
  } else {
    return false;
  }
}

/**
 * Get active filters from query string
 *
 * @return array An array of active filters
 */
function get_active_filters() {
  $authorFilter = isset($_GET['author']) ? $_GET['author'] : NULL;
  $topicFilter = isset($_GET['topic']) ? $_GET['topic'] : NULL;
  $typeFilters = isset($_GET['post_type']) ? $_GET['post_type'] : NULL;
  $filtersArray = [
    'author' => $authorFilter,
    'topic' => $topicFilter,
    'type' => $typeFilters
  ];
  $filteredArray = array_filter($filtersArray);
  if ($filteredArray) {
    return $filtersArray;
  } else {
    return false;
  }
}

/**
 * Generate clear filters link
 * @param  string Search query
 *
 * @return string Clear filters link
 */
function clear_filters_link($query) {
  return home_url() . '/search/?query=' . $query;
}

/**
 * Generate results heading
 * @param  array $filteredResults Array of filtered results
 * @param  int   $totalResults    The total number of results
 * @param  int   $perPage         Number of results per page
 * @param  int   $paged           Current page
 *
 * @return string                 Results heading
 */
function generate_results_heading($filteredResults, $totalResults, $perPage, $paged) {
  if ($perPage >= $totalResults) {
    return $totalResults;
  } else {
    $filteredResultCount = count($filteredResults);
    $pageFirstResult = ($perPage * $paged) - ($perPage - 1);
    $pageLastResult = ($perPage * $paged) - $perPage + $filteredResultCount;
    if($pageFirstResult == $pageLastResult) {
      return $pageLastResult . ' of ' . $totalResults;
    } else {
      return $pageFirstResult . '-' . $pageLastResult . ' of ' . $totalResults;
    }
  }
}

// Template Name: Search
$context = Timber::get_context();

// Search query
$query = $params['query'];
$formattedQuery = stripslashes($query);

// Pagination parameters
$paged = $params['page'];
$perPage = $params['per_page'];

// Generate search results
// filteredResults _must_ be called before unfilteredResults
$filteredResults = search_results_from_query($query, $perPage, $paged, true);
// These calls must be made before `search_results_from_query` is called again
// Extract the total found_posts from the global wp_query magic
$totalResults = $wp_query->found_posts;
// Same with pagination
$pagination = Timber::get_pagination();
$unfilteredResults = search_results_from_query($query, -1, $paged);

// Get active filters
get_active_filters();

// Build context
$context['query'] = $formattedQuery;
$context['page_title'] = generate_page_title($formattedQuery);
$context['total_results'] = $totalResults;
$context['results_heading'] = generate_results_heading($filteredResults, $totalResults, $perPage, $paged);
$context['per_page'] = $perPage;
$context['results'] = Timber::get_posts($filteredResults, 'MonashImpact\Decorators\Article');
$context['authors'] = build_author_filter($unfilteredResults);
$context['topics'] = build_topic_filter($unfilteredResults);
$context['types'] = build_type_filter($unfilteredResults);
$context['active_filters'] = get_active_filters();
$context['clear_filters_link'] = clear_filters_link($query);
$context['pagination'] = $pagination;

Timber::render('search/show.twig', $context);
