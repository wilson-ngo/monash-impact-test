<?php
/* Template Name: Categories */

$context = Timber::get_context();
$context['page_title'] = $params['title'];

$category_args = [
  'post_type' => 'category',
  'posts_per_page' => -1,
  'orderby' => 'title',
  'order' => 'ASC'
];
$categorys = Timber::get_posts($category_args, 'MonashImpact\Decorators\Category');

// Assemble context
$context['categories'] = $categorys;
if($context['categories']) {
  Timber::render('categories/index.twig', $context, 10800, cache_method());
} else {
  render_error('404');
}
