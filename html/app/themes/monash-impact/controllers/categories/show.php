<?php
/* Template Name: Categories */

$context = Timber::get_context();

// Fetch category by $params["slug"]
$category_args = [
  'post_type' => 'category',
  'posts_per_page' => 1,
  'name'  => $params['slug']
];
$category = Timber::get_post($category_args, 'MonashImpact\Decorators\Category');

if($category) {
  $categoryID = $category->ID;

  // Set default value for `page` variable
  if (!isset($params['page']) || !$params['page']){
    $params['page'] = 1;
  }

  // Fetch articles with category
  $articles_args = [
    'post_type' => 'article',
    'meta_key' => 'category',
    'meta_value' => $categoryID,
    'meta_compare' => 'LIKE',
    'posts_per_page' => 16,
    'paged' => $params['page']
  ];
  query_posts($articles_args);
  $articles = Timber::get_posts($articles_args, 'MonashImpact\Decorators\Article');

  // Assemble context
  $context['category'] = $category;
  $context['articles'] = $articles;
  $context['page_title'] = $category->post_title;
  $context['pagination'] = Timber::get_pagination(array('format' => 'page/%#%'));
  Timber::render('categories/show.twig', $context, 10800, cache_method());
} else {
  render_error('404');
}
