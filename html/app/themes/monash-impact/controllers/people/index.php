<?php

/* Template Name: People */

$context = Timber::get_context();
$context['page_title'] = $params['title'];

$args = [
  'post_type' => 'person',
  'posts_per_page' => $params['per_page'],
  'paged' => $params['page'],
  'orderby' => 'title',
  'order' => 'ASC',
  'meta_key' => 'show_on_people_page',
  'meta_value' => 1,
];

// THIS LINE IS CRUCIAL
// https://github.com/timber/timber/wiki/Pagination
// in order for WordPress to know what to paginate
// your args have to be the default query
// make sure you've got query_posts in your .php file
query_posts($args);
// END
$context['people'] = Timber::get_posts($args, 'MonashImpact\Decorators\Person');
$context['pagination'] = Timber::get_pagination(array('format' => 'page/%#%'));
if($context['people']) {
  Timber::render('people/index.twig', $context, 10800, cache_method());
} else {
  render_error('404');
}
