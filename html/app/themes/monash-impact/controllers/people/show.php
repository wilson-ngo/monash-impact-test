<?php
/* Template Name: People */

$context = Timber::get_context();

// Fetch person by $params["slug"]
$person_args = [
  'post_type' => 'person',
  'posts_per_page' => 1,
  'name'  => $params['slug']
];
$person = Timber::get_post($person_args, 'MonashImpact\Decorators\Person');

if ($person) {

  function show_single_row_of_articles($personID) {
    $singleRowOfArticles = get_field('single_row_of_articles', $personID);
    if ($singleRowOfArticles) {
      return true;
    } else {
      return false;
    }
  }

  function articles_per_page($personID) {
    $showSingleRow = show_single_row_of_articles($personID);
    if ($showSingleRow) {
      return 4;
    } else {
      return 16;
    }
  }

  $personID = $person->ID;
  $singleRowOfArticles = show_single_row_of_articles($personID);

  // Set default value for `page` variable
  if (!isset($params['page']) || !$params['page']){
    $params['page'] = 1;
  }

  // Fetch articles by person
  $articles_args = [
    'post_type' => 'article',
    'meta_query' => [
      'relation' => 'OR',
      [
        'key' => 'authors',
        'value' => $personID,
        'compare' => 'LIKE',
      ],
      [
        'key' => 'academics',
        'value' => $personID,
        'compare' => 'LIKE',
      ],
    ],
    'posts_per_page' => articles_per_page($personID),
    'paged' => $params['page']
  ];
  query_posts($articles_args);
  $articles = Timber::get_posts($articles_args, 'MonashImpact\Decorators\Article');

  // Assemble context
  $context['person'] = $person;
  $context['page_title'] = $person->post_title;
  $context['articles'] = $articles;
  if (!$singleRowOfArticles) {
    $context['pagination'] = Timber::get_pagination(array('format' => 'page/%#%'));
  }
  Timber::render('people/show.twig', $context, 10800, cache_method());
} else {
  render_error('404');
}

