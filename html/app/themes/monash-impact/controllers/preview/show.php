<?php
/* Template Name: Preview articles */
$context = Timber::get_context();
$args = [
  'post_type' => 'article',
  'p' => $params['id']
];
$context['article'] = Timber::get_post($args, 'MonashImpact\Decorators\Article');
if($context['article']) {
  $context['page_title'] = $context['article']->post_title;
}

if($context['article']) {
  Timber::render('articles/show.twig', $context);
} else {
  render_error('404');
}
