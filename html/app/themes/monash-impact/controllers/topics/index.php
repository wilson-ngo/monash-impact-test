<?php
/* Template Name: Topics */

$context = Timber::get_context();
$context['page_title'] = $params['title'];

$topic_args = [
  'post_type' => 'topic',
  'posts_per_page' => -1,
  'orderby' => 'title',
  'order' => 'ASC'
];
$topics = Timber::get_posts($topic_args, 'MonashImpact\Decorators\Topic');

// Assemble context
$context['topics'] = $topics;
if($context['topics']) {
  Timber::render('topics/index.twig', $context, 10800, cache_method());
} else {
  render_error('404');
}
