<?php
/* Template Name: Topics */

$context = Timber::get_context();

// Fetch topic by $params["slug"]
$topic_args = [
  'post_type' => 'topic',
  'posts_per_page' => 1,
  'name'  => $params['slug']
];
$topic = Timber::get_post($topic_args, 'MonashImpact\Decorators\Topic');

if($topic) {
  $topicID = $topic->ID;

  // Set default value for `page` variable
  if (!isset($params['page']) || !$params['page']){
    $params['page'] = 1;
  }

  // Fetch articles with topic
  $articles_args = [
    'post_type' => 'article',
    'meta_key' => 'topics',
    'meta_value' => $topicID,
    'meta_compare' => 'LIKE',
    'posts_per_page' => 16,
    'paged' => $params['page']
  ];
  query_posts($articles_args);
  $articles = Timber::get_posts($articles_args, 'MonashImpact\Decorators\Article');

  // Assemble context
  $context['topic'] = $topic;
  $context['articles'] = $articles;
  $context['page_title'] = $topic->post_title;
  $context['pagination'] = Timber::get_pagination(array('format' => 'page/%#%'));
  Timber::render('topics/show.twig', $context, 10800, cache_method());
} else {
  render_error('404');
}
