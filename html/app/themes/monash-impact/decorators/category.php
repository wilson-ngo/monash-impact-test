<?php

namespace MonashImpact\Decorators;
use Timber;

class Category extends Timber\Post {
  public function has_description() {
    if($this->get_field('description')) {
      return true;
    }
  }
  public function description() {
    return $this->get_field('description');
  }
  public function latest_articles() {
    $articles_args = [
      'post_type' => 'article',
      'posts_per_page' => 4,
      'meta_key' => 'category',
      'meta_value' => $this->ID,
      'meta_compare' => 'LIKE'
    ];
    return Timber::get_posts($articles_args, 'MonashImpact\Decorators\Article');
  }
  public function has_articles() {
    $articles = $this->latest_articles();
    $numberOfArticles = count($articles);
    if ($numberOfArticles > 0) {
      return true;
    }
  }
}
