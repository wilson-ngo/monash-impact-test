<?php

namespace MonashImpact\Decorators;
use Timber;
use TimberImage;
use Timber\ImageHelper;
use MonashImpact\RichTextModifier;
use MonashImpact\Decorators\Person;
use MonashImpact\Decorators\Topic;

class Article extends Timber\Post {
  public function has_hero_image() {
    if($this->get_field('hero_image')) {
      return true;
    }
  }
  public function hero_image_background_color() {
    $heroImageBackgroundColor = $this->get_field('hero_image_background_color');
    return $heroImageBackgroundColor ? $heroImageBackgroundColor : '#333';
  }
  public function hero_image_src() {
    $image = $this->get_field('hero_image');
    $timberImage = new TimberImage($image);
    return Timber\ImageHelper::resize($timberImage, 1440, 700, 'center');
  }
  public function thumbnail_src() {
    $image = $this->get_field('hero_image');
    $timberImage = new TimberImage($image);
    return Timber\ImageHelper::resize($timberImage, 270, 180, 'center');
  }
  public function open_graph_src() {
    $image = $this->get_field('hero_image');
    $timberImage = new TimberImage($image);
    return Timber\ImageHelper::resize($timberImage, 1200, 630, 'center');
  }
  public function has_hero_image_caption() {
    if($this->get_field('hero_image_caption')) {
      return true;
    }
  }
  public function hero_image_caption() {
    return $this->get_field('hero_image_caption');
  }
  public function hero_image_full_width() {
    if($this->get_field('hero_image_full_width')) {
      return true;
    }
  }
  public function hero_image_gradient_overlay() {
    if($this->get_field('hero_image_gradient_overlay')) {
      return true;
    }
  }
  public function type() {
    return $this->get_field('type');
  }
  public function type_plural() {
    return $this->type . 's';
  }
  public function is_type_article() {
    if($this->get_field('type') == 'Article') {
      return true;
    }
  }
  public function is_type_video() {
    if($this->get_field('type') == 'Video') {
      return true;
    }
  }
  public function link() {
    return home_url() . '/' . strtolower($this->type_plural()) . '/' . $this->slug;
  }
  public function has_authors() {
    if($this->get_field('authors')) {
      return true;
    }
  }
  public function authors() {
    return array_map(function ($author) {
      return new Person($author);
    }, $this->get_field('authors'));
  }
  public function author_names() {
    return implode(', ', $this->authors());
  }
  public function authors_heading() {
    $authors = $this->get_field('authors');
    $numberOfAuthors = count($authors);
    return $numberOfAuthors > 1 ? 'Authors' : 'Author';
  }
  public function has_academics() {
    if($this->get_field('academics')) {
      return true;
    }
  }
  public function academics() {
    return array_map(function ($academic) {
      return new Person($academic);
    }, $this->get_field('academics'));
  }
  public function academics_heading() {
    $academics = $this->get_field('academics');
    $numberOfAcademics = count($academics);
    return $numberOfAcademics > 1 ? 'Featured academics' : 'Featured academic';
  }
  public function has_category() {
    if($this->get_field('category')) {
      return true;
    }
  }
  public function category() {
    $categories = $this->get_field('category');
    $category = $categories[0];
    return new Category($category);
  }
  public function has_topics() {
    if($this->get_field('topics')) {
      return true;
    }
  }
  public function topics() {
    return array_map(function ($topic) {
      return new Topic($topic);
    }, $this->get_field('topics'));
  }
  public function primary_topic() {
    return $this->topics[0];
  }
  public function theme() {
    return str_replace(' ', '-', strtolower($this->primary_topic));
  }
  public function has_intro() {
    if($this->get_field('intro')) {
      return true;
    }
  }
  public function intro() {
    return $this->get_field('intro');
  }
  public function body() {
    $rich_text_modifier = new RichTextModifier(
      $this->get_field('body')
    );
    return $rich_text_modifier->render();
  }
  public function republish_body() {
    $rich_text_modifier = new RichTextModifier(
      $this->get_field('body'),
      "partials/embedded/republication"
    );
    return $rich_text_modifier->render();
  }
  public function has_tags() {
    if($this->get_field('tags')) {
      return true;
    }
  }
  public function tags() {
    $has_tags = $this->has_tags();
    if ($has_tags) {
      return array_map(function ($tag) {
        return new Tag($tag);
      }, $this->get_field('tags'));
    }
  }
  public function tag_list() {
    $has_tags = $this->has_tags();
    if ($has_tags) {
      return implode(', ', $this->tags());
    }
  }
  public function twitter_share_url() {
    return 'https://twitter.com/home?status=' . $this->link;
  }
  public function facebook_share_url() {
    return 'https://www.facebook.com/sharer/sharer.php?u=' . $this->link;
  }
  public function linkedin_share_url() {
    return 'https://www.linkedin.com/shareArticle?mini=true&url=' . $this->link . '&title=' . $this->name;
  }
  public function has_summary() {
    if($this->get_field('summary')) {
      return true;
    }
  }
  public function summary() {
    return $this->get_field('summary');
  }
  public function open_graph_type() {
    return strtolower($this->get_field('type'));
  }
  public function open_graph_description() {
    if ($this->has_summary()) {
      return $this->summary();
    } elseif ($this->has_intro()) {
      return $this->intro();
    } else {
      return $this->body();
    }
  }
}
