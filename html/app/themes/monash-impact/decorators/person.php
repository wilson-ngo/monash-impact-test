<?php

namespace MonashImpact\Decorators;
use Timber;
use TimberImage;
use Timber\ImageHelper;

class Person extends Timber\Post {
  public function has_bio() {
    if($this->get_field('bio')) {
      return true;
    }
  }
  public function bio() {
    return $this->get_field('bio');
  }
  public function has_role() {
    if($this->get_field('role')) {
      return true;
    }
  }
  public function role() {
    return $this->get_field('role');
  }
  public function has_social_media_links() {
    if($this->get_field('twitter_username') || $this->get_field('linkedin') ||  $this->get_field('url') || $this->get_field('email_address')) {
      return true;
    }
  }
  public function twitter_url() {
    return "http://twitter.com/" . $this->twitter_username;
  }
  public function twitter_handle() {
    return "@" . $this->twitter_username;
  }
  public function linkedin() {
    return $this->get_field('linkedin');
  }
  public function email_address() {
    return $this->get_field('email_address');
  }
  public function has_avatar() {
    if($this->get_field('avatar')) {
      return true;
    }
  }
  public function avatar_thumbnail_src() {
    $image = $this->get_field('avatar');
    $timberImage = new TimberImage($image);
    return Timber\ImageHelper::resize($timberImage, 60, 60, 'center');
  }
  public function avatar_src() {
    $image = $this->get_field('avatar');
    $timberImage = new TimberImage($image);
    return Timber\ImageHelper::resize($timberImage, 180, 180, 'center');
  }
  public function open_graph_src() {
    $image = $this->get_field('avatar');
    $timberImage = new TimberImage($image);
    return Timber\ImageHelper::resize($timberImage, 180, 180, 'center');
  }
  public function open_graph_description() {
    if ($this->has_bio()) {
      return $this->bio;
    } else {
      return 'Biography and articles by ' . $this->name;
    }
  }
}
