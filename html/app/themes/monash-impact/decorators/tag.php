<?php

namespace MonashImpact\Decorators;
use Timber;

class Tag extends Timber\Term {
  public function link() {
    return home_url() . '/tags/' . $this->slug;
  }
}
