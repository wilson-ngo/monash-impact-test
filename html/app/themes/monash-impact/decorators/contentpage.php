<?php

namespace MonashImpact\Decorators;
use Timber;
use MonashImpact\RichTextModifier;

class ContentPage extends Timber\Post {
  public function body() {
    $rich_text_modifier = new RichTextModifier(
      $this->get_field('body')
    );
    return $rich_text_modifier->render();
  }
}
