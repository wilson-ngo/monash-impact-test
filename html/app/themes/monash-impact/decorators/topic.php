<?php

namespace MonashImpact\Decorators;
use Timber;

class Topic extends Timber\Post {
  public function latest_articles() {
    $articles_args = [
      'post_type' => 'article',
      'posts_per_page' => 4,
      'meta_key' => 'topics',
      'meta_value' => $this->ID,
      'meta_compare' => 'LIKE'
    ];
    return Timber::get_posts($articles_args, 'MonashImpact\Decorators\Article');
  }
  public function has_articles() {
    $articles = $this->latest_articles();
    $numberOfArticles = count($articles);
    if ($numberOfArticles > 0) {
      return true;
    }
  }
}
