<?php

namespace MonashImpact;

use DOMDocument;
use Embed\Embed;
use Timber;
use TimberImage;
use Underscore\Types\Arrays;
use Underscore\Types\Strings;

class RichTextModifier {

  protected $doc;
  protected $templatePrefix = "partials/embedded";
  protected $xmlPragma = '<?xml encoding="utf-8" ?>';
  protected $wrapperStart = '<div id="rtm-wrapper">';
  protected $wrapperEnd = '</div>';

  /**
   * Constructor
   *
   * Set up DOMDocument instance var and trigger a parse.
   *
   * @param [type] $html_string
   */
  public function __construct($html_string = "", $templatePrefix = "") {
    $this->doc = new DOMDocument();
    libxml_use_internal_errors(true);
    $this->doc->encoding = 'utf-8';
    $this->parse($html_string);
    if (!empty($templatePrefix)) {
      $this->templatePrefix = $templatePrefix;
    }
  }

  /**
   * Parse HTML into the main DOMDocument instance var. Wraps the base HTML in a
   * wrapping div so we can easily find the content.
   *
   * @param string $html_string
   * @return void
   */
  public function parse($html_string) {
    $html_string = $this->xmlPragma . $this->wrapperStart . $html_string . $this->wrapperEnd;
    // Try to force UTF-8 everywhere
    $html_string = mb_convert_encoding($html_string, 'HTML-ENTITIES', 'UTF-8');
    // Parse string into DOMDocument
    // Ensure it loads without any extraneous bits: doctype, html element, body, etc
    $this->doc->loadHTML(
      $html_string,
      LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
    );
  }

  /**
   * Render
   *
   * Trigger clean and cleanup of content within the main $this->doc
   *
   * @return void
   */
  public function render() {
    $this->clean();
    return $this->unwrap(
      $this->doc->saveHTML()
    );
  }

  /**
   * Clean the DOM
   *
   * Traverse the top-level nodes and, based on some naive matching rules, pass
   * those nodes off to specific cleaning methods.
   *
   * @return void
   */
  private function clean() {
    $wrapper = $this->doc->childNodes[1];
    // Copy array since we’re going to mutate
    $children = array();
    for ($i = 0; $i < $wrapper->childNodes->length; $i++) {
      $node = $wrapper->childNodes->item($i);
      array_push($children, $node);
    }
    // Iterate over copy
    for ($i = 0; $i < sizeof($children); $i++) {
      $node = $children[$i];
      // Match blockquotes
      if ($node->nodeName == "blockquote") {
        $this->cleanBlockquote($node);
      }
      // Check content of <p>s, they can contain stuff
      if ($node->nodeName == "p") {
        // Match images without captions
        $first_child = $node->childNodes[0];
        $second_child = $first_child->childNodes[0];
        if ($first_child->nodeName == "img" || ($first_child->nodeName == "a" && $second_child->nodeName == "img")) {
          $this->cleanImageWithoutCaption($node);
        }
        // If only a text node, and text matches a URL
        if (
          $first_child->nodeName == "#text" &&
          Strings::isUrl($first_child->textContent)
        ) {
          $this->cleanUrl($node);
        }
      }
      // Check content of <div>s, they can contain stuff
      if ($node->nodeName == "div") {
        // Has id of form "attachment_"
        $node_id = $node->getAttribute("id");
        // Match images with captions
        if (Strings::startsWith($node_id, "attachment_")) {
          $this->cleanImageWithCaption($node);
        }
      }
    }
  }

  /**
   * Clean and mutate an image without caption in place
   *
   * Extract the meta-information relating to an image, renders it through our
   * normal Twig setup, and replaces the top-level node with the result.
   *
   * <p>
   *  <img class="alignnone wp-image-105 size-medium" src="..." alt=""
   *  width="300" height="225" srcset="..." sizes="(max-width: 300px) 100vw,
   *  300px">
   * </p>
   *
   * @param DOMElement $node
   * @return void
   */
  private function cleanImageWithoutCaption($node) {
    // Check for `a` element wrapping the image
    if($node->childNodes[0]->tagName == 'a') {
      $link = $node->childNodes[0];
      $link_href = $link->getAttribute("href");
      $image = $link->childNodes[0];
    } else {
      $image = $node->childNodes[0];
    }
    $classes = explode(
      " ",
      $image->getAttribute("class")
    );
    // Extract media ID
    $image_class = Arrays::find($classes, function($classname) {
      return Strings::startsWith($classname, "wp-image-");
    });
    $image_id = (Integer) Strings::remove($image_class, "wp-image-");

    // Extract alignment
    $alignment_class = Arrays::find($classes, function($classname) {
      return Strings::startsWith($classname, "align");
    });
    $alignment = Strings::remove($alignment_class, "align");

    // Extract size
    $size_class = Arrays::find($classes, function($classname) {
      return Strings::startsWith($classname, "size-");
    });
    $size = Strings::remove($size_class, "size-");

    // Extract width and height
    $width = $image->getAttribute("width");
    $height = $image->getAttribute("height");

    return $this->replaceImage($node, array(
      "id" => $image_id,
      "alignment" => $alignment,
      "size" => $size,
      "width" => $width,
      "height" => $height,
      "link" => isset($link_href) ? $link_href : NULL
    ));
  }

  /**
   * Clean and mutate an image with caption in place
   *
   * Extract the meta-information relating to an image, renders it through our
   * normal Twig setup, and replaces the top-level node with the result.
   *
   * <div id="attachment_105" style="width: 310px" class="wp-caption alignnone">
   *  <img class="wp-image-105 size-medium" src="..." alt="" width="300"
   *  height="225" srcset="..." sizes="(max-width: 300px) 100vw, 300px"> <p
   *  class="wp-caption-text"> I am the caption now. </p> </div>
   *
   * @param DOMElement $node
   * @return void
   */
  private function cleanImageWithCaption($node) {
    // Check for `a` element wrapping the image
    if($node->childNodes[0]->tagName == 'a') {
      $link = $node->childNodes[0];
      $link_href = $link->getAttribute("href");
      $image = $link->childNodes[0];
    } else {
      $image = $node->childNodes[0];
    }
    $caption = $node->childNodes[1];
    $node_classes = explode(
      " ",
      $node->getAttribute("class")
    );
    $image_classes = explode(
      " ",
      $image->getAttribute("class")
    );
    // Extract media ID
    $image_class = Arrays::find($image_classes, function($classname) {
      return Strings::startsWith($classname, "wp-image-");
    });
    $image_id = (Integer) Strings::remove($image_class, "wp-image-");

    // Extract alignment
    $alignment_class = Arrays::find($node_classes, function($classname) {
      return Strings::startsWith($classname, "align");
    });
    $alignment = Strings::remove($alignment_class, "align");

    // Extract size
    $size_class = Arrays::find($image_classes, function($classname) {
      return Strings::startsWith($classname, "size-");
    });
    $size = Strings::remove($size_class, "size-");

    // Extract width and height
    $width = $image->getAttribute("width");
    $height = $image->getAttribute("height");

    return $this->replaceImage($node, array(
      "id" => $image_id,
      "alignment" => $alignment,
      "size" => $size,
      "width" => $width,
      "height" => $height,
      "caption" => $caption->textContent,
      "link" => isset($link_href) ? $link_href : NULL
    ));
  }

  /**
   * Clean and mutate a blockquote in place
   *
   * Simply looks for alignment values in `style` attrs and pulls that up to the
   * level of the blockquote itself.
   *
   * @param DOMElement $blockquote
   * @return void
   */
  private function cleanBlockquote($blockquote) {
    // We only care about alignment
    $alignment = "center";
    for ($i = 0; $i < $blockquote->childNodes->length; $i++) {
      $node = $blockquote->childNodes->item($i);
      // This seems really stupid
      if($node->nodeType == XML_ELEMENT_NODE) {
        $style_attr = $node->getAttribute("style");
        if ($style_attr == "text-align: right;") {
          $alignment = "right";
        } else if ($style_attr == "text-align: left;") {
          $alignment = "left";
        }
        // Remove the attribute no matter what
        $node->removeAttribute("style");
      }
    }
    // Add class to blockquote
    $data = array(
      "alignment" => $alignment,
      "content" => $this->innerHTML($blockquote),
    );
    $element = $this->htmlToDomElement(
      Timber::fetch($this->templatePrefix . "/blockquote.twig", $data)
    );
    $blockquote->parentNode->replaceChild($element, $blockquote);
  }

  private function cleanUrl($node) {
    $url = $node->textContent;
    try {
      $oembed = Embed::create($url);
      // Don't replace if we don't get a code
      $code = $oembed->getCode();
      if (!empty($code)) {
        $data = array(
          "title" => $oembed->getTitle(),
          "type" => $oembed->getType(),
          "url" => $oembed->getUrl(),
          "provider" => $oembed->getProviderName(),
          "width" => $oembed->getWidth(),
          "height" => $oembed->getHeight(),
          "code" => $code
        );
        $element = $this->htmlToDomElement(
          Timber::fetch($this->templatePrefix . "/oembed.twig", $data)
        );
        $node->parentNode->replaceChild($element, $node);
      }
    } catch (\Embed\Exceptions\InvalidUrlException $e) {
      // Swallow exception
    }
  }

  /**
   * Replace image node with Twig-rendered version
   *
   * @param DOMElement $node
   * @param array $data
   * @return void
   */
  private function replaceImage($node, $data) {
    $data["image"] = new TimberImage($data["id"]);
    $element = $this->htmlToDomElement(
      Timber::fetch($this->templatePrefix . "/image.twig", $data)
    );
    $node->parentNode->replaceChild($element, $node);
  }

  /**
   * Convert HTML string to a DOMElement
   *
   * @param string $string
   * @return DOMElement within the main $this->doc
   */
  private function htmlToDomElement($string) {
    $doc = new DOMDocument();
    $doc->encoding = 'UTF-8';
    $doc->loadHTML(
      $string,
      LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
    );
    libxml_clear_errors();
    return $this->doc->importNode(
      $doc->childNodes[0],
      true
    );
  }

  /**
   * Unwrap the start/end wrapper from the main $this->doc
   *
   * @param string $string
   * @return string
   */
  private function unwrap($string) {
    // Incredibly naive way of stripping the wrapper
    return substr(
      trim($string),
      strlen($this->xmlPragma) + strlen($this->wrapperStart),
      strlen($this->wrapperEnd) * -1
    );
  }

  /**
   * Return inner HTML of passed node
   *
   * @param DOMElement $node
   * @return string
   */
  private function innerHTML($node) {
    $output = "";
    for ($i = 0; $i < $node->childNodes->length; $i++) {
      $output = $output . $this->doc->saveHTML(
        $node->childNodes->item($i)
      );
    }
    return mb_convert_encoding($output, 'HTML-ENTITIES', 'UTF-8');
  }
}
