<?php
  function create_article_post_type() {
    $args = array(
      'labels' => array(
        'name' => __('Articles'),
        'singular_name' => __('Article')
      ),
      'public'             => true,
      'publicly_queryable' => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'capability_type'    => 'post',
      'has_archive'        => false,
      'rewrite'            => array('slug' => 'articles', 'with_front' => false),
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array('title'),
      'menu_icon'          => 'dashicons-groups',
    );
    register_post_type('article', $args);
  }
  add_action('init', 'create_article_post_type');

  // Create custom meta box with preview button
  function create_article_preview_meta_box($post) {
    global $post;
    $status = $post->post_status;
    if (isset($status) && ($status == 'draft' || $status == 'auto-draft')) {
      add_meta_box('article-preview', 'Preview', 'article_preview_meta_box', 'article', 'side', 'high');
    }
  }
  add_action('add_meta_boxes', 'create_article_preview_meta_box');

  // Generate preview button
  function article_preview_meta_box($post) {
    $previewLink = get_home_url() .'/preview/articles/' . $post->ID;
    echo '<a class="button button--large" id="post-preview" href="' . $previewLink . '" target="wp-preview-' . (int) $post->ID . '">Preview</a>';
    echo '<input type="hidden" name="wp-preview" id="wp-preview" value="" />';
  }

  // Adjust preview link
  function article_preview_link() {
    global $post;
    return get_home_url() . '/preview/articles/' . $post->ID;
  }
  add_filter('preview_post_link', 'article_preview_link');

  function track_post_views($post) {
    // Only run the process for single posts, pages and post types when the user is logged out
    if ( is_singular() && !is_user_logged_in() ) {
      // global $post;
      $custom_field = '_post_view_count';

      // Set/check session
      if ( !session_id() )
        session_start();

      // Only track a one view per post for a single visitor session to avoid duplications
      if ( !isset( $_SESSION["popular-posts-count-{$post->ID}"] ) ) {

        // Update view count
        $view_count = get_post_meta( $post->ID, $custom_field, true );
        $stored_count = ( isset($view_count) && !empty($view_count) ) ? ( intval($view_count) + 1 ) : 1;
        $update_meta = update_post_meta( $post->ID, $custom_field, $stored_count );

        // Check for errors
        if ( is_wp_error($update_meta) )
          error_log( $update_meta->get_error_message(), 0 );

        // Store session in "viewed" state
        $_SESSION["popular-posts-count-{$post->ID}"] = 1;
      }

      // Show view the count for testing purposes (add "?show_count=1" onto the URL)
      if ( isset($_GET['show_count']) && intval($_GET['show_count']) == 1 ) {
        echo '<p style="color:red; text-align:center; margin:1em 0;">';
        echo get_post_meta( $post->ID, $custom_field, true );
        echo ' views of this post</p>';
      }
    }
  }
