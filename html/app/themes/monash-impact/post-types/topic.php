<?php
  function create_topic_post_type() {
    $args = array(
      'labels' => array(
        'name' => __('Topics'),
        'singular_name' => __('Topic')
      ),
      'public'             => true,
      'publicly_queryable' => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'capability_type'    => 'post',
      'has_archive'        => false,
      'rewrite'            => array('slug' => 'topics', 'with_front' => false),
      'hierarchical'       => true,
      'menu_position'      => null,
      'supports'           => array('title', 'page-attributes'),
      'menu_icon'          => 'dashicons-groups',
    );
    register_post_type('topic', $args);
  }
  add_action('init', 'create_topic_post_type');
