<?php
  function create_person_post_type() {
    $args = array(
      'labels' => array(
        'name' => __('People'),
        'singular_name' => __('Person')
      ),
      'public'             => true,
      'publicly_queryable' => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'capability_type'    => 'post',
      'has_archive'        => false,
      'rewrite'            => array('slug' => 'people', 'with_front' => false),
      'hierarchical'       => true,
      'menu_position'      => null,
      'supports'           => array('title', 'page-attributes'),
      'menu_icon'          => 'dashicons-groups',
    );
    register_post_type('person', $args);
  }
  add_action('init', 'create_person_post_type');
