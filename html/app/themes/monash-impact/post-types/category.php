<?php
  function create_category_post_type() {
    $args = array(
      'labels' => array(
        'name' => __('Categories'),
        'singular_name' => __('Category')
      ),
      'public'             => true,
      'publicly_queryable' => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'capability_type'    => 'post',
      'has_archive'        => false,
      'rewrite'            => array('slug' => 'departments', 'with_front' => false),
      'hierarchical'       => true,
      'menu_position'      => null,
      'supports'           => array('title', 'page-attributes'),
      'menu_icon'          => 'dashicons-groups',
    );
    register_post_type('category', $args);
  }
  add_action('init', 'create_category_post_type');
