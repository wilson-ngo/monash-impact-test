<?php
  function create_content_page_post_type() {
    $args = array(
      'labels' => array(
        'name' => __('Content pages'),
        'singular_name' => __('Content page')
      ),
      'public'             => true,
      'publicly_queryable' => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'capability_type'    => 'post',
      'has_archive'        => false,
      'rewrite'            => array('slug' => '', 'with_front' => false),
      'hierarchical'       => true,
      'menu_position'      => null,
      'supports'           => array('title', 'page-attributes'),
      'menu_icon'          => 'dashicons-groups',
    );
    register_post_type('content_page', $args);
  }
  add_action('init', 'create_content_page_post_type');
