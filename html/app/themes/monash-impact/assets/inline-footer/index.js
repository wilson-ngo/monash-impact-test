import FontFaceObserver from "fontfaceobserver";

/**
 * List of fonts we care about loading
 * @type {Array}
 */
var fontsToCheck = [
  {
    family: "Roboto Condensed",
    style: "normal",
    weight: 400
  },
  {
    family: "Roboto Condensed",
    style: "italic",
    weight: 400
  }
];

/**
 * Font loading
 * Implements deferred font-loading
 * https://www.filamentgroup.com/lab/font-events.html
 */

function fontsLoaded(fontsToCheck) {
  var timeout = 1500;
  var fontsLoaded = window.localStorage.getItem("fontsLoaded");

  if (!fontsLoaded) {
    var loading = fontsToCheck.map(function(font) {
      const observer = new FontFaceObserver(font.family, {
        weight: font.weight,
        style: font.style
      });
      return observer.load(null, timeout);
    });
    Promise.all(loading).then(
      function() {
        window.document.documentElement.className += " fonts-loaded";
        window.localStorage.setItem("fontsLoaded", true);
      },
      function() {
        window.document.documentElement.className += " fonts-loaded";
      }
    );
  }
}

fontsLoaded(fontsToCheck);
