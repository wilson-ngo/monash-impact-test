import "metaquery";
import { loadCSS } from "fg-loadcss";
import checkFontsPreloaded from "./check-fonts-preloaded";
require("fg-loadcss/src/cssrelpreload.js");
// Expose as a global
window.loadCSS = loadCSS;
checkFontsPreloaded();
