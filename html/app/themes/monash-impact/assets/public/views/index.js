import copyToClipboard from "../copy-to-clipboard";
import latestTweets from "../latest-tweets";
import toggleClass from "../toggle-class";

const views = {
  copyToClipboard,
  latestTweets,
  toggleClass
};

export default views;
