import twitterFetcher from "twitter-fetcher";
import mergeDefaults from "../utilities/merge-defaults";

export default function latestTweets(el, props) {
  const defaults = {
    enableLinks: true,
    lang: "en",
    maxTweets: props.numberOfTweets,
    profile: {
      screenName: props.handle
    },
    showImages: false,
    showInteraction: false,
    showRetweet: false,
    showTime: true,
    showUser: false
  };

  const config = mergeDefaults({}, defaults, props);

  function renderTweets(tweets) {
    const output = tweets.map(tweet => {
      return "<div class='tweet-block__tweet'>" + tweet + "</div>";
    });
    el.innerHTML = output.join("");
  }

  config.customCallback = renderTweets;
  twitterFetcher.fetch(config);
}
