import Clipboard from "clipboard";
import addClass from "../utilities/add-class";
import removeClass from "../utilities/remove-class";

/**
 * Copy the contents of an element to the clipboard
 * @param  {Node}   el    The parent element
 * @param  {Object} props Various options:
 *                        - {String} triggerEl
 *                        - {String} targetEl
                          - {String} tooltipEl
 * @return {Void}
 */
export default function copyToClipboard(el, props) {
  const triggerEl = el.querySelector(props.triggerElement);
  const targetEl = el.querySelector(props.targetElement);
  const tooltipEl = el.querySelector(props.tooltipElement);
  const activeClass = props.tooltipActiveClass;
  const clipboard = new Clipboard(triggerEl, {
    target: function() {
      return targetEl;
    }
  });
  clipboard.on("success", function(e) {
    e.clearSelection();
    // Show a confirmation tooltip?
    if (tooltipEl) {
      toggleTooltip(tooltipEl, activeClass);
    }
  });
}

/**
 * Toogle the tooltip element
 * @param  {Node}   el          The tooltip element
 * @param  {string} activeClass Tooltip is visible class
 * @return {Void}
 */
function toggleTooltip(el, activeClass) {
  addClass(el, activeClass);
  setTimeout(function() {
    removeClass(el, activeClass);
  }, 400);
}
