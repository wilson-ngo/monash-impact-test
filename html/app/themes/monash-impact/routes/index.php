<?php
  // Refresh once with this uncommented to flush all the permalinks
  // global $wp_rewrite;
  // $wp_rewrite->init();
  // $wp_rewrite->flush_rules();

  /**
   * Home
   */
  Routes::map('/', function($params) {
    Routes::load('home.php', null, null, 200);
  });

  /**
   * Articles
   */
  Routes::map('/articles', function($params) {
    $params_for_view = array(
      'page' => 1,
      'per_page' => 16,
      'title' => 'Articles'
    );
    Routes::load('controllers/articles/index.php', $params_for_view, null, 200);
  });

  Routes::map('/articles/page/:page', function($params) {
    $params_for_view = array(
      'page' => $params['page'],
      'per_page' => 16,
      'title' => 'All articles'
    );
    Routes::load('controllers/articles/index.php', $params_for_view, null, 200);
  });

  Routes::map('/articles/:slug', function($params) {
    Routes::load('controllers/articles/show.php', $params, null, 200);
  });

  /**
   * Podcasts
   */
  Routes::map('/podcasts', function($params) {
    $params_for_view = array(
      'page' => 1,
      'per_page' => 16,
      'title' => 'Podcasts'
    );
    Routes::load('controllers/podcasts/index.php', $params_for_view, null, 200);
  });

  Routes::map('/podcasts/page/:page', function($params) {
    $params_for_view = array(
      'page' => $params['page'],
      'per_page' => 16,
      'title' => 'All podcasts'
    );
    Routes::load('controllers/podcasts/index.php', $params_for_view, null, 200);
  });

  Routes::map('/podcasts/:slug', function($params) {
    Routes::load('controllers/podcasts/show.php', $params, null, 200);
  });

  /**
   * Infographics
   */
  Routes::map('/infographics', function($params) {
    $params_for_view = array(
      'page' => 1,
      'per_page' => 16,
      'title' => 'Infographics'
    );
    Routes::load('controllers/infographics/index.php', $params_for_view, null, 200);
  });

  Routes::map('/infographics/page/:page', function($params) {
    $params_for_view = array(
      'page' => $params['page'],
      'per_page' => 16,
      'title' => 'All infographics'
    );
    Routes::load('controllers/infographics/index.php', $params_for_view, null, 200);
  });

  Routes::map('/infographics/:slug', function($params) {
    Routes::load('controllers/infographics/show.php', $params, null, 200);
  });

  /**
   * Videos
   */
  Routes::map('/videos', function($params) {
    $params_for_view = array(
      'page' => 1,
      'per_page' => 16,
      'title' => 'Videos'
    );
    Routes::load('controllers/videos/index.php', $params_for_view, null, 200);
  });

  Routes::map('/videos/page/:page', function($params) {
    $params_for_view = array(
      'page' => $params['page'],
      'per_page' => 16,
      'title' => 'All videos'
    );
    Routes::load('controllers/videos/index.php', $params_for_view, null, 200);
  });

  Routes::map('/videos/:slug', function($params) {
    Routes::load('controllers/videos/show.php', $params, null, 200);
  });

  /**
   * By topic
   */
  Routes::map('/topics', function($params) {
    $params_for_view = array(
      'title' => 'Topics'
    );
    Routes::load('controllers/topics/index.php', $params_for_view, null, 200);
  });

  Routes::map('/topics/:slug/page/:page', function($params) {
    $params_for_view = array(
      'slug' => $params['slug'],
      'page' => $params['page']
    );
    Routes::load('controllers/topics/show.php', $params_for_view, null, 200);
  });

  Routes::map('/topics/:slug', function($params) {
    Routes::load('controllers/topics/show.php', $params, null, 200);
  });

  /**
   * By category
   */
  Routes::map('/departments', function($params) {
    $params_for_view = array(
      'title' => 'Departments'
    );
    Routes::load('controllers/categories/index.php', $params_for_view, null, 200);
  });

  Routes::map('/departments/:slug/page/:page', function($params) {
    $params_for_view = array(
      'slug' => $params['slug'],
      'page' => $params['page']
    );
    Routes::load('controllers/categories/show.php', $params_for_view, null, 200);
  });

  Routes::map('/departments/:slug', function($params) {
    Routes::load('controllers/categories/show.php', $params, null, 200);
  });

  /**
   * By tag
   */
  Routes::map('/tags/:slug/page/:page', function($params) {
    $params_for_view = array(
      'slug' => $params['slug'],
      'page' => $params['page']
    );
    Routes::load('controllers/tags/show.php', $params_for_view, null, 200);
  });

  Routes::map('/tags/:slug', function($params) {
    Routes::load('controllers/tags/show.php', $params, null, 200);
  });

  /**
   * People
   */
  Routes::map('/people', function($params) {
    $params_for_view = array(
      'page' => 1,
      'per_page' => 12,
      'title' => 'Authors'
    );
    Routes::load('controllers/people/index.php', $params_for_view, null, 200);
  });

  Routes::map('/people/page/:page', function($params) {
    $params_for_view = array(
      'page' => $params['page'],
      'per_page' => 12,
      'title' => 'Authors'
    );
    Routes::load('controllers/people/index.php', $params_for_view, null, 200);
  });

  Routes::map('/people/:slug/page/:page', function($params) {
    $params_for_view = array(
      'slug' => $params['slug'],
      'page' => $params['page']
    );
    Routes::load('controllers/people/show.php', $params_for_view, null, 200);
  });

  Routes::map('/people/:slug', function($params) {
    Routes::load('controllers/people/show.php', $params, null, 200);
  });

  /**
   * Search
   */
  Routes::map('/search', function($params) {
    $query = isset($_GET['query']) ? $_GET['query'] : NULL;
    $params_for_view = array(
      'query' => $query,
      'page' => 1,
      'per_page' => 20,
    );
    Routes::load('controllers/search/show.php', $params_for_view, null, 200);
  });

  Routes::map('/search/page/:page', function($params) {
    $query = isset($_GET['query']) ? $_GET['query'] : NULL;
    $params_for_view = array(
      'query' => $query,
      'page' => $params['page'],
      'per_page' => 20,
    );
    Routes::load('controllers/search/show.php', $params_for_view, null, 200);
  });

  /**
   * Article preview
   */
  Routes::map('/preview/articles/:id', function($params) {
    $params_for_view = array(
      'id' => $params['id']
    );
    Routes::load('controllers/preview/show.php', $params_for_view, null, 200);
  });
