<?php

/**
  * This file is multi-function. It needs to:
  *
  * - Handle the home page
  * - Check for content_page types
  * - 404 everything else
  */
$context = Timber::get_context();
$current_page = find_page();
if ($_SERVER["REQUEST_URI"] == "/impact/") {
  // Featured article
  $context['featured_article'] = find_featured_article();
  // Row of 4 articles
  $context['row_of_articles'] = find_row_of_articles();
  // Spotlight on
  $spotlightTopic = get_field('spotlight_on', 'options');
  $context['spotlight_articles'] = find_spotlight_articles('4');
  $context['spotlight_title'] = $spotlightTopic[0]->post_title;
  $context['spotlight_link'] =  '/topics/' . $spotlightTopic[0]->post_name;
  // Popular articles
  $context['popular_articles'] = popular_articles();
  // Fetch and cache events
  $context['upcoming_events'] = TimberHelper::transient( 'upcoming_events', function(){
    return fetch_events(4);
  }, 10800); // Cache for 3 hours
  // Render home page
  Timber::render('home.twig', $context);
} else if ($current_page) {
  // Render matching page based on slug
  $context['page'] = $current_page;
  $context['page_title'] = $current_page->post_title;
  Timber::render('pages/show.twig', $context);
} else {
  render_error('404');
}

function find_page () {
  $url_segments = explode("/", trim($_SERVER["REQUEST_URI"], "/"));
  if (empty($url_segments[0])) {
    return false;
  }
  $current_slug = end($url_segments);
  $query_args = array(
    'post_type' => 'content_page',
    'name' => $current_slug
  );
  return Timber::get_post($query_args, 'MonashImpact\Decorators\ContentPage');
}

function find_featured_article() {
  // Grab featured article ID from custom field
  $featuredArticle = get_field('featured_article', 'options');
  $featuredArticleID = $featuredArticle[0]->ID;
  // Query featured article
  $query_args = array(
    'post_type' => 'article',
    'p' => $featuredArticleID
  );
  // Return TimberPost
  return Timber::get_post($query_args, 'MonashImpact\Decorators\Article');
}

function find_row_of_articles() {
  $articles = get_field('row_of_articles', 'options');
  // Return TimberPost
  return Timber::get_posts($articles, 'MonashImpact\Decorators\Article');
}

function popular_articles() {
  $args = [
    'post_type' => 'article',
    'posts_per_page' => 4,
    'paged' => 1,
    'meta_query' => [
      'relation' => 'OR',
      [
        'key' => 'type',
        'compare' => 'IN',
        'value' => ['article', 'infographic', 'podcast', 'video']
      ],
      [
        'key' => '_post_view_count',
        'value' => '1',
        'compare' => 'NOT EXISTS'
      ],
      'post_views' => [
        'key' => '_post_view_count',
      ]
    ],
    'orderby' => 'post_views',
    'order' => 'DESC'
  ];
  return Timber::get_posts($args, 'MonashImpact\Decorators\Article');
}

function find_spotlight_articles($number) {
  // Grab spotlight topic ID from custom field
  $spotlightTopic = get_field('spotlight_on', 'options');
  $spotlightTopicID = $spotlightTopic[0]->ID;
  // Query latest articles by topic
  $query_args = array(
    'post_type' => 'article',
    'posts_per_page' => $number,
    'meta_key' => 'topics',
    'meta_value' => $spotlightTopicID,
    'meta_compare' => 'LIKE'
  );
  // Return TimberPost
  return Timber::get_posts($query_args, 'MonashImpact\Decorators\Article');
}

function fetch_events($total) {
  $return = array();
  $html = fetch_document('https://www.monash.edu/business/news-and-events/events');
  $events = $html->find('.box-listing-element__events-item');
  for($i=0; $i < $total; $i++){
    $event = $events[$i];
    $start_at = DateTime::createFromFormat('Y-m-d H:i P', $event->find('.dt-start')[0]->getAttribute('datetime'));

    $start_date = $start_at->format('jS M Y');

    if ($start_at->format('g:ia') != '12:00am') {
      $start_date = $start_date.' '.$start_at->format('g:ia');
    }

    array_push($return,
      array(
        'name' => $event->find('h4')[0]->plaintext,
        'start' => $start_date,
        'url' => $event->find('a')[0]->href
      )
    );
  }
  return $return;
}

function fetch_document($url) {
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_HEADER, 0);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
  curl_setopt($curl, CURLOPT_URL, $url);
  $html=curl_exec($curl);
  $dom = new simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
  $html=$dom->load($html, true, true);
  return $html;
}
