<?php
/*
Plugin Name:  Webpack assets
Plugin URI:   https://github.com/icelab/icelab-assets
Description:  Icelab webpack assets for WordPress.
Version:      1.0.0
Author:       Icelab
Author URI:   https://icelab.com.au
*/

namespace Icelab\Assets;

/**
 * Get paths for assets
 */
class JsonManifest {
  private $manifest;

  /**
   * Constructor
   * Fetch the manifest file if it exists
   */
  public function __construct($manifest_path) {
    if (file_exists($manifest_path)) {
      $this->manifest = json_decode(file_get_contents($manifest_path), true);
    } else {
      $this->manifest = [];
    }
  }

  public function get() {
    return $this->manifest;
  }

  /**
   * Return the manifested path for a given `$key`
   */
  public function getPath($key = '', $default = null) {
    $collection = $this->manifest;
    if (is_null($key)) {
      return $collection;
    }
    if (isset($collection[$key])) {
      return $collection[$key];
    }
    foreach (explode('.', $key) as $segment) {
      if (!isset($collection[$segment])) {
        return $default;
      } else {
        $collection = $collection[$segment];
      }
    }
    return $collection;
  }
}

/*
 * Asset path helper
 *
 * Resolve the path to a webpack asset based on the development environment. In
 * development we return full URLs to the webpack-dev-server, and in production
 * we return the served-path to production-built assets.
 *
 * @param {String} $filename Path to the file to load. For top-level target
 * outputs (JS and CSS) this is likely to be
 * `#{monash-impact}__#{entry-name}.*`, and for the files within those targets,
 * `#{monash-impact}/assets/#{entry-name}/path/to/file.ext`.
 * @param {Boolean} $full_path Return the _full_ file path for an asset.
 * @param {Boolean} $with_domain Return the full domain as part of the generated
 * URL (based on `WP_HOME`)
 *
 */
function asset_path($filename, $full_path = false, $with_domain = false) {
  if (WP_ENV == 'production') {
    $dist_path = WP_CONTENT_DIR . '/assets/';
    $public_path = '/app/assets/';
    static $manifest;

    if (empty($manifest)) {
      $manifest_path = WP_CONTENT_DIR . '/assets/' . 'asset-manifest.json';
      $manifest = new JsonManifest($manifest_path);
    }

    // Read the full path or the public path?
    $path_prefix = ($full_path) ? $dist_path : $public_path;
    $domain_prefix = ($with_domain) ? WP_HOME : "";

    if (array_key_exists($filename, $manifest->get())) {
      return $domain_prefix . $path_prefix . $manifest->get()[$filename];
    } else {
      return $domain_prefix . $path_prefix . $filename;
    }
  } else {
    return WP_ASSETS . "/assets/" . $filename;
  }
}
