module.exports = {
  module: {
    rules: [
      {
        test: require.resolve("fg-loadcss"),
        use: ["imports-loader?this=>window&module=>0"]
      },
      {
        test: require.resolve("fg-loadcss/src/cssrelpreload.js"),
        use: ["imports-loader?this=>window&module=>0"]
      }
    ]
  }
};
