# Monash Impact

Impact WordPress website for Monash Business School.

Primary developers:

* Andy McCray

This app is based on our [WordPress skeleton](https://github.com/icelab/wordpress-skeleton), which is itself built on top of [Bedrock](https://roots.io/bedrock/).

## Specific documentation

* [Deployment](docs/deployment.md)
* [Server setup](docs/server.md)

## Development
### First-time setup

You’ll need [Docker](https://docs.docker.com/docker-for-mac/) up and running, then you can:

```
script/setup
```

This occasionally hangs for reasons unknown, though it still finishes correctly. Once it’s built you should be able to call:

```
script/server
```

And then view the site at `http://0.0.0.0:3000` where you’ll be asked to do the standard WordPress installation.

### Environment variables

You’ll want to check the environment variables in your `.env` file:

* `DB_NAME` — Your local database name
* `DB_USER` — Your local database user, e.g. `root`
* `DB_PASSWORD` — Your local database password
* `DB_HOST` — Your local database host, e.g. `localhost`
* `WP_HOME` — The URL you'll be serving the site (see below)
* `ENABLE_CACHE` — This can be `false` in development

### Development database

A database dump exists inside the repository.

#### Import
```
mysql -uroot -p --host=127.0.0.1 -P13306 wordpress < ./db.sql
```

#### Export
```
mysqldump wordpress -h 127.0.0.1 -u root --password="password" --port=13306 > ./db.sql
```

### Admin

The WordPress admin interface is located at `http://localhost:3000/wp/wp-admin`. Login credentials are in 1Password.

### Plugins

WordPress plugins can be installed using composer. We include [WordPress Packagist](https://wpackagist.org/) as a composer repository, so you have access to any of the plugins you can find there. To add one include it in your `composer.json` and then run:

```
script/run web "composer update"
```

### Scripts

There are a couple of scripts for development you can run. These generally conform to [GitHub’s script-to-rule-them-all approach](https://github.com/github/scripts-to-rule-them-all).

* `script/bootstrap` simply checks that your dependencies are filled (docker in this case).
* `script/setup` will set the project up from a clean start.
* `script/update` will update the app after a fresh pull.
* `script/server` is used to start the application, by default it will run `script/update` which you can ignore with a `NO_UPDATE=true` flag.
* `script/run` can be used to run commands on the various containers that make up the application. `script/run web bash` will get you to a bash shell in main `web` container.

### App structure

Bedrock isolates _project specific_ files into a directory called `html/app`. WordPress core files live in `html/wp` and aren’t checked into the project repository.

You will also need to generate and provide key values as instructed in the `.env` file.

### Assets

Assets are built using our standard [icelab-assets](https://github.com/icelab/icelab-assets) setup. It’s configured to look for assets in `html/app/themes/*` and will find any `entry.js` files in there to generate build entries for. Entries can generate JavaScript, CSS, and file types that are referenced from within each entry context.

You can create a new entry like this:

```
npm run create-entry apps/themes/monash-impact/assets/:entry-name
```

In development, docker-compose will spin up a local asset server that will serve the bundled assets. This server runs over <http://localhost:8080> and assets are served from <http://localhost:8080/assets/output-file-name.ext>. When you first run the server, it should spit out a list of the top-level entry files that it’s generating.

In production, we generate a hashed version of each output file into `/html/app/assets` on deploy. This ensure that the latest deployed version will always get the right assets associated with it. This is important to note because it means you *must* reference assets using the method described below.

#### App dependencies

If you add a new `npm` dependency you’ll need to ensure it gets installed within your `assets` container during development. To do this run:

```
script/run assets "npm install'
```

#### Referencing assets in templates

When referencing assets in templates, you should _always_ use the `asset_path` helper to ensure that the asset will be linked correctly in both development and production environments. In the Timber/Twig set up we have here, you need to wrap the function call like so:

```html
<link rel="stylesheet" href="{{asset_path('monash-impact__entry-name.css')}}" />
```

You can also inline an asset output thusly:

```html
<script>{{include(asset_path('monash-impact__inline-header.js', true))}}</script>
```

To reference sub-resources of an entry in your templates you’ll need to include the full path to that file from the base theme directory:

```
<img src="{{asset_path('monash-impact/assets/entry-name/header/logo.png'}}/>
```
